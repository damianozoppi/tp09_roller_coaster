/**
 * 
 * Train class calls the methods load and unload for permission of each
 *  customer to get board or  get unboard
 * Concurrent Programming - TP9
 * 
 * @author      Roberto Ramos-Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @author 		Damiano Zoppi <Damiano.Zoppi@edu.hefr.ch>
 * @version     1.7          
 * @since       2012-06-01
 *
 * 
 *
 */

import java.util.Random;


public class Train implements Runnable{
	
	// time constants for the trip duration and the preparation of the train
	static final int DURATION_TRIP_MIN = 5000;
	static final int DURATION_TRIP_MAX = 6000;
	static final int TRAIN_PREPARATION_TIME = 1000;
	
	static private Random randomGenerator = new Random();	
	String id = new String("UNDEF");
	
	//We created this variable in order to identify eventually more trains
	public Train(String id){
		this.id = id;
	}
	
	public void run(){
		try{
			while(true){
				RollerCoaster.monitor.load();			// the train permits customers to board
				//Random duration time of the trip
				Thread.sleep(DURATION_TRIP_MIN + 
						randomGenerator.nextInt(DURATION_TRIP_MAX - DURATION_TRIP_MIN));
				RollerCoaster.monitor.unload();			// the train permits customers to unboard
				Thread.sleep(TRAIN_PREPARATION_TIME);	//Preparation time
				System.out.println("The train is preparing for next trip");
			}
		}
		catch (InterruptedException e) {
			e.printStackTrace();
        }
	}
}
