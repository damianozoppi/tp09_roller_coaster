/**
 * 
 * Monitor class (Methods and conditions); the Monitor class will be giving the necesary methods 
 * to the customer and to the train whenever they need it.
 * 
 * Concurrent Programming - TP9
 * 
 * @author      Roberto Ramos-Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @author 		Damiano Zoppi <Damiano.Zoppi@edu.hefr.ch>
 * @version     1.7          
 * @since       2012-06-01
 *
 * 
 *
 */

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Monitor {
	
	final Lock lock = new ReentrantLock();
		
	// Train conditions
	final Condition train_ready =  lock.newCondition();		// The train has all passenger aboard
		
	// Customers conditions
	final Condition board_permission = lock.newCondition(); 	// Customers can board the train
	final Condition unboard_permission = lock.newCondition();	// Customers can unboard the train
	
	final Condition animation = lock.newCondition();
	
	final static int SEATS = 6;
	
	//Counters when the customers are in the train and freeSeats when ever are off the train
	int customersOnBoard = 0;
	int freeSeats = SEATS;
	
	boolean loadCalled = false;
	boolean unloadCalled = false;

//*******************************Methods called by the Train*************************************//
	
	public void load() throws InterruptedException{
		lock.lock();
		try{
			loadCalled = true;					// load() is called --> customers can board the train
			System.out.println("!! Customers can get on the train !!");
			while(freeSeats < SEATS){
				board_permission.signal();		// signals all customers to board
				freeSeats++;
			}
			train_ready.await();				// wait the train becomes full
			System.out.println("=== Train departed ===>");
		}
		finally{
			lock.unlock();
		}
	}
	
	public void unload() throws InterruptedException{	
		lock.lock();
		try{
			loadCalled = false;			//It does not allow the train to call the method load 
			System.out.println("<=== Train arrived ===");
			System.out.println("!! Customers can get off the train !!");
			while(freeSeats > 0){		
				unboard_permission.signal();	// signals all customers to unboard
				freeSeats--;
			}
		}
		finally{
			lock.unlock();
		}
	}
	
	
//*******************************Methods called by the Customer*************************************///
	
	public void board(Customer c) throws InterruptedException{
		lock.lock();
		try{
			while(!loadCalled){
				board_permission.await();		//It does not allow the entry of customers before 
												//the train call the method load
			}
			System.out.println(c.name + " wants a ride");	
			if(customersOnBoard < SEATS){
				System.out.println(">> " + c.name + " boarding" );
				customersOnBoard++;
				if(customersOnBoard == SEATS){
					System.out.println(">> " + c.name + " is the last customer to board" );
					System.out.println("--------------------");
					System.out.println("     TRAIN FULL     ");
					System.out.println("--------------------");
					train_ready.signal(); 		// train is full --> signals that it's ready
				}
			}
			else{
				System.out.println(c.name + " waiting for the train to come");
			}
		}
		finally{
			lock.unlock();
		}		
	}
	
	public void unboard(Customer c) throws InterruptedException{
		lock.lock();
		try{
			unboard_permission.await();		//when signaled the customers are allow to leave the train
			if(customersOnBoard > 0){
				System.out.println("<< " + c.name + " is unboarding" );
				customersOnBoard--;
				if(customersOnBoard == 0){
					System.out.println("<< " + c.name + " is the last customer to unboard" );
					System.out.println("---------------------");
					System.out.println("     TRAIN EMPTY     ");
					System.out.println("---------------------");
				}
			}
		}
		finally{
			lock.unlock();
		}	
	}
	
}
