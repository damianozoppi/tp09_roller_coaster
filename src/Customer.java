/**
 * 
 * Customer class has the two necessary methods for be able to board and to unboard the train
 * for each of the customers created in the main class RollerCoaster.
 * 
 * Concurrent Programming - TP9
 * 
 * @author      Roberto Ramos-Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @author 		Damiano Zoppi <Damiano.Zoppi@edu.hefr.ch>
 * @version     1.7          
 * @since       2012-06-01
 *
 * 
 *
 */

import java.util.Random;



public class Customer implements Runnable {
	
	// Decisions time constant for the first an the next trip
	static final int FRIST_DECIDE_TIME_MIN = 1000;
	static final int FIRST_DECIDE_TIME_MAX = 10000;
	static final int DECIDE_TIME_MIN = 20000;
	static final int DECIDE_TIME_MAX = 30000;
	
	//String name that will be save the names of the customer from names.txt
	String name = new String("UNDEF");
	static private Random randomGenerator = new Random();

	
	public Customer(String name){
		this.name = name;
	}
	
	public void run(){
		boolean firstTimeCustomer = true;
		try{
			while(true){
				//Checks for the first waiting time of each customer
 				if(firstTimeCustomer){
					Thread.sleep(FRIST_DECIDE_TIME_MIN + 
							randomGenerator.nextInt(FIRST_DECIDE_TIME_MAX - FRIST_DECIDE_TIME_MIN)); 
					firstTimeCustomer = false;
				}
				//Checks for the next waiting time of each customer
				else{
					Thread.sleep(DECIDE_TIME_MIN + 
							randomGenerator.nextInt(DECIDE_TIME_MAX - DECIDE_TIME_MIN));
				}
				RollerCoaster.monitor.board(this);		// customers tries to enter the train
				RollerCoaster.monitor.unboard(this);	// customer gets off the train	
			}
		}
		catch (InterruptedException e) {
			e.printStackTrace();
        }
	}
}
