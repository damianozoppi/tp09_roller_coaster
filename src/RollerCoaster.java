/**
 * 
 * RollerCoaster is the main class of the program that, it will create all objects train, 
 * RollerCoaster(monitor) and the number of needed clients. Also it will receive the names
 *  of each customer from a external text. 
 * Concurrent Programming - TP9
 * 
 * @author      Roberto Ramos-Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @author 		Damiano Zoppi <Damiano.Zoppi@edu.hefr.ch>
 * @version     1.7       
 * @since       2012-05-31
 * @time 		15 hrs
 *
 * 
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class RollerCoaster {
	
	public static ExecutorService threadExecutor = Executors.newCachedThreadPool();
	public static Monitor monitor = new Monitor();	//Monitor creation
	
	final static int NUMBER_CLIENTS_PARK = 20;
	
	//This is method will start the program
	public static void main(String []args){
		String name;
	    File inFile = new File("names.txt");	//Gets the name of each customer 
	    Scanner in;
		try{
			in = new Scanner(inFile);
			Train train = new Train("TRAIN");	//Train creation
			Customer customers[] = new Customer[NUMBER_CLIENTS_PARK];
			threadExecutor.execute(train);
			for (int i = 0; i < NUMBER_CLIENTS_PARK; i++) {
                name = in.nextLine();
                customers[i] = new Customer(name);	//Creation of all customers
                threadExecutor.execute(customers[i]);
            }
		} catch(FileNotFoundException e){
			e.printStackTrace();
		}	
	}
}
